<?php
	require_once 'connect.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>UsicMoney</title>
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="dist/css/default.date.css" id="theme_date">
	<!-- Loading main style -->
	    <link href="dist/css/style.css" rel="stylesheet">
	<!-- Loading Bootstrap -->
	    <link href="dist/css/vendor/bootstrap.min.css" rel="stylesheet">
	<!-- Loading Flat UI -->
	    <link href="dist/css/flat-ui.css" rel="stylesheet">
    <link href="docs/assets/css/demo.css" rel="stylesheet">
</head>

<body style="background: #37353A; color:#fff;">
<!-- menu -->
          <nav  class="navbar navbar-inverse navbar-embossed" role="navigation" style="background:#322E34;">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <img src="dist/img/logo.png">
              </button>
              <a class="navbar-brand" href="#" style="padding:0px 150px;"><img src="dist/img/logo.png"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-01">
              <ul class="nav navbar-nav navbar-left" style="float:right!important;">
                <li class="active"><a href="#fakelink">Внести кошти<span class="navbar-unread">1</span></a></li>
                 <li><a href="#fakelink">Інкасація</a></li>               
               </ul>
            </div><!-- /.navbar-collapse -->
          </nav>

<div class="container">

	<div class="moneyIn" style="width: 450px; float:left; border-right: 1px solid #78C8D3;">
		<?include ("action.php");?>
		<form method="post" prc_adsf="true" class="mainMoney">
		<ul class="inputMoney" style="margin-bottom:0px;"> 
			<li><input type="text" value="" placeholder="*First name" required class="form-control" name="first_name" /></li>
			<li><input type="text" value="" placeholder="*Last name " required class="form-control" name="last_name" /></li>
			<li><input type="text" value="" placeholder="*Cash" required class="form-control" name="cash" /></li>
			<li>
				<select data-toggle="select" class="form-control select select-info mrs mbm" name="type">
				    <option value="Принтер">Принтер</option>
				    <option value="Чай">Чай</option>
				</select>
			</li>
			<li><input type="text" value="<?php echo date("Y-m-d H:i:s");?>" placeholder="Time " required class="form-control" name="time" /></li>
			<li><input type="text" value="" placeholder="Note" class="form-control" name="note" /></li>
			<li><center><input class="btn btn-success" type="submit" value="Ввести дані"></center></li>
		</ul>
	  	</form>
	</div>

	<div class="lastData" style="width:600px; float:right; padding-top:85px;">
		<div class="printer" style="padding-left:150px; padding-top:40px; height:130px; width:650px; 	background-image: url(dist/img/printerBg.png); background-repeat: no-repeat;"> 
		<?php
			$result = mysql_query('SELECT * FROM zimmer WHERE type="Принтер" ORDER BY id DESC LIMIT 1 ');
			echo '<table  class="lastPrinter last">';
			echo '<tbody>';
			while($data = mysql_fetch_array($result)){ 
				echo '<tr>';
				echo '<td>' . $data['name'] . '</td>';
				echo '<td>' . $data['surname'] . '</td>';
				echo '<td>' . $data['cash'] . ' грн. </td>';
				echo '<td>' . $data['date'] . '</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		?>
		</div><!--/printer-->
		<div style="width:650px; height:60px; padding:13px;">
			
			<a href="#" style="float:right;">
				<span style="font-size:18px; margin:0px; padding-right:10px;">детальніше</span>
				<img src="dist/img/Arrowhead-Right-01-32 (1).png">
			</a>
		</div>
		<div class="tea" style="padding-left:150px; padding-top:50px; height:130px; width:650px; 	background-image: url(dist/img/teaBg.png); background-repeat: no-repeat;"> 
		<?php
			$result = mysql_query('SELECT * FROM zimmer WHERE type="Чай" ORDER BY id DESC LIMIT 1 ');
			echo '<table  class="lastPrinter last">';
			echo '<tbody>';
			while($data = mysql_fetch_array($result)){ 
				echo '<tr>';
				echo '<td>' . $data['name'] . '</td>';
				echo '<td>' . $data['surname'] . '</td>';
				echo '<td>' . $data['cash'] . ' грн. </td>';
				echo '<td>' . $data['date'] . '</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		?>
		</div><!--/tea-->
	</div>

</div><!--/Container-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../dist/js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../dist/js/flat-ui.min.js"></script>

    <script src="../assets/js/application.js"></script>

</body>
</html>
