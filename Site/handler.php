<?php
	// Этот файл был определен в предыдущей статье
	require_once 'connect.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Основы PHP и MySQL</title>
<style>
* { font-family:Lato }
</style>
</head>
<body>
<?php
// Получить данные из суперглобального массива $_POST и обработать их
	$email = isset($_POST['email']) ? 
		trim(mysqli_real_escape_string($link, $_POST['email'])) : '';
	
	if (!empty($email)) {
		$link->query("INSERT INTO newsletter(email) VALUES ('{$email}')");
		
		// Проверяем прошла ли операция (свойство affected_rows возвращает число строк, 
		// затронутых предыдущей операцией MySQL (в нашем случае 1)
		if ($link->affected_rows == 1)
			echo '<h1>Спасибо за подписку</h1>';
		else
			echo '<p>Что-то пошло не так при попытке записи вашего email в базу данных</p>';
	}
?>